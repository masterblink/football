<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_team');
            $table->string('nombre');
            $table->string('codigo');
            $table->integer('fundacion')->nullable();
            $table->string('colores')->nullable();
            $table->string('estadio')->nullable();
            $table->string('email')->nullable();
            $table->string('website')->nullable();
            $table->timestamps();
            /*
            1772,Chapecoense AF,CHA,1973,Green / White,Arena Condá,comunicacao@chapecoense.com,http://www.chapecoense.com
            */
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teams');
    }
}
