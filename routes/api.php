<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/', 'API\CompetenciasAPIController@getCompetencias')->name('competencias.show');

Route::post('teams/save', 'API\TeamsAPIController@saveTeams')->name('teams.save');

Route::post('players/save', 'API\PlayersAPIController@savePlayers')->name('players.save');
