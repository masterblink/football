<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('competitions/');
});

Route::get('competitions/', 'CompetenciasController@index')->name('competencias.index');
Route::get('competitions/{id}', 'CompetenciasController@getDetail')->name('competencias.detail');

Route::get('teams/', 'TeamsController@index')->name('teams.index');
Route::get('teams/{id}', 'TeamsController@getDetail')->name('teams.detail');

Route::get('players/', 'PlayersController@index')->name('players.index');
