<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Player;

class PlayersAPIController extends Controller
{
    public function savePlayers(Request $request){

        //dd(request()->datos);
        $datos = (array) json_decode(request()->datos);//dd($datos);
        foreach($datos as $item){
            
            $teamsave = explode(',',$item->item);
            if(!empty($teamsave[0])){
            
                $filtro = [
                    'id_player' => $teamsave[0]
                ];

                $team = Player::firstOrNew($filtro);

                if(!$team->exists){     
                    
                    $team->id_player = $teamsave[0];
                }
                $team->nombre = $teamsave[1];            
                $team->position = $teamsave[2];
                $team->camiseta =$teamsave[3];                          
        
                $team->save();
            }
        }

        return response()->json([
            'msg' => "Jugadores guardados correctamente"
            
        ], 200);
        //$team = Team::firstOrNew($filtroPaciente);

    }
}


