<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Team;

class TeamsAPIController extends Controller
{
    public function saveTeams(Request $request){

        //dd(request()->datos);
        $datos = (array) json_decode(request()->datos);//dd($datos);
        foreach($datos as $item){
            
            $teamsave = explode(',',$item->item);
            if(!empty($teamsave[0])){
            
                $filtro = [
                    'id_team' => $teamsave[0]
                ];

                $team = Team::firstOrNew($filtro);

                if(!$team->exists){     
                    
                    $team->id_team = $teamsave[0];
                }
                $team->nombre = $teamsave[1];            
                $team->codigo = $teamsave[2];
                $team->fundacion =$teamsave[3];
                $team->colores = $teamsave[4];
                $team->estadio = $teamsave[5];
                $team->email = $teamsave[6];
                $team->website = $teamsave[7];            
        
                $team->save();
            }
        }

        return response()->json([
            'msg' => "Equipos guardados correctamente"
            
        ], 200);
        //$team = Team::firstOrNew($filtroPaciente);

    }
}
