<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

class CompetenciasAPIController extends Controller
{
    public function getCompetencias(Request $request){

        $client = new Client();
    	$response = $client->request('GET', 'https://api.football-data.org/v2/competitions');
    	$statusCode = $response->getStatusCode();
    	$body = $response->getBody()->getContents();

    	return $body;

    }
}
