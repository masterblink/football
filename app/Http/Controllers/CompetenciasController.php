<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class CompetenciasController extends Controller
{
    
    private $credentials;

    public function __construct() {
		$this->credentials = 'c6e8835774a94b44b238074c2b61281f';
	}

    public function index(){

        $client = new Client();
    	$response = $client->request('GET', 'https://api.football-data.org/v2/competitions');
    	$statusCode = $response->getStatusCode();
    	$body = (array) json_decode($response->getBody()->getContents());
        
        return view('home',compact('body'));

    }

    public function getDetail(Request $request){

        try{
            $client = new Client();
            $response = $client->request(
                'GET', 
                "https://api.football-data.org/v2/competitions/" . request()->id,
                [
                    'headers' => ['X-Auth-Token' => $this->credentials]
                ]);
            $statusCode = $response->getStatusCode();
            $detail = (array) json_decode($response->getBody()->getContents());
            
            $detail['teams'] = $this->getTeams(request()->id);
            $detail['players'] = $this->getPlayers(request()->id);
            
            
        } catch (\Exception $e) {

            //return back()->withError($e->getMessage())->withInput();

            $error = $e->getMessage();

            return view('detail',compact('error'));

            
        }

        return view('detail',compact('detail'));

    }

    public function getTeams($id){

        $client = new Client();
        
    	$response = $client->request(
            'GET', 
            "https://api.football-data.org/v2/competitions/" . request()->id . "/teams",
            [
                'headers' => ['X-Auth-Token' => $this->credentials]
            ]);
    	$statusCode = $response->getStatusCode();
    	$teams = (array) json_decode($response->getBody()->getContents());
        
        return $teams;

    }

    public function getPlayers($id){

        $client = new Client();
        
    	$response = $client->request(
            'GET', 
            "https://api.football-data.org/v2/competitions/" . request()->id . "/scorers",
            [
                'headers' => ['X-Auth-Token' => $this->credentials]
            ]);
    	$statusCode = $response->getStatusCode();
    	$scorers = (array) json_decode($response->getBody()->getContents());
        
        return $scorers;

    }
}
