<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Team;


class TeamsController extends Controller
{
    public function index(){

        $equipos = Team::all();

        return view('teams',compact('equipos'));
    }

    public function getDetail(Request $request){

        $detail = Team::where('id_team', request()->id)->get();

        return view('teamdetail',compact('detail'));
    }
}
