<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Player;

class PlayersController extends Controller
{
    public function index(){

        $jugadores = Player::all();

        return view('players',compact('jugadores'));
    }
}



