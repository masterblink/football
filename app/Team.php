<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $table = 'teams';

    protected $fillable = [	'id_team',	'nombre',	'codigo',	'fundacion',	'colores',	'estadio',	'email',	'website',	'created_at',	'updated_at'
];
}
