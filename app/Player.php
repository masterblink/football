<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    protected $table = 'players';

    protected $fillable = ['id_player','nombre','position','camiseta','created_at','updated_at'];
}
