@extends('layout')

@section('title','Equipo')

@section('content')

<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
      <h1 class="h3 mb-0 text-gray-800">Detalle Equipo</h1>
      
    </div>

    <!-- Content Row -->
    <div class="row">
        <div class="col-lg-6 mb-4">
            <!--
            
            
            -->
            <!-- Illustrations -->
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">{{ $detail[0]['nombre']}}</h6>
                </div>
                <div class="card-body">
                <div class="text-center">
                   
                </div>
                <p>
                        
                        Código: {{ $detail[0]["codigo"]}}<br>
                        Fundacion: {{ $detail[0]["fundacion"]}}<br>
                        Colores: {{ $detail[0]["colores"]}}<br>
                        Estadio: {{ $detail[0]["estadio"]}}<br>
                        Email: {{ $detail[0]["email"]}}<br>
                        Web: {{ $detail[0]["website"]}}<br>
                        
                </p>
                </div>
            </div>

        </div>
    </div>
    

  </div>

@endsection

