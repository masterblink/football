@extends('layout')

@section('title','Jugadores')

@section('content')

<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
      <h1 class="h3 mb-0 text-gray-800">Jugadores</h1>
      
    </div>

    <!-- Content Row -->
    
    <div class="row">
        <div class="col-lg-12 mb-4">

            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Jugadores</h6>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                    <table class="table table-bordered" id="players" width="90%" cellspacing="0">
                        <thead>
                        <tr>
                            
                            <th>Nombre</th>
                            <th>Posicion</th>
                            <th>Nro Camiseta</th>
                        </tr>
                        </thead>
                        
                        <tbody>
                        @foreach ($jugadores as $item)
                        <tr data-entry-id="{{ $item->id_player }}">
                            
                            <td>{{ $item->nombre }}</td>
                            <td>{{ $item->position}}</td>
                            <td>{{ $item->camiseta }}</td>
                        </tr>
                        @endforeach  
                        
                        </tbody>
                        
                        <tfoot>
                                
                            </tfoot>
                    </table>
                    </div>
                </div>
                
            </div>
        </div>
        
    </div>

  </div>

@endsection

@section('javascript')
<script>




$(function(){

    

    $('#players').DataTable();
    
    
})

</script>
@endsection