@extends('layout')

@section('title','Competencias')

@section('content')

<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
      <h1 class="h3 mb-0 text-gray-800">Competencias</h1>
      <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
    </div>

    <!-- Content Row -->
    
    <div class="row">
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Competencias</h6>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="90%" cellspacing="0">
                      <thead>
                        <tr>
                          <th>Area</th>  
                          <th>Nombre</th>
                          <th>Codigo</th>
                          <th>Plan</th>
                          <th>Emblema</th>
                          <th>Start date</th>
                          <th>Salary</th>
                        </tr>
                      </thead>
                      
                      <tbody>
                        @foreach ($body['competitions'] as $item)
                        <tr data-entry-id="{{ $item->id }}">
                            <td>{{ $item->area->name }}</td>
                            <td><a href="{{ route('competencias.detail',['id' => $item->id])}}">{{ $item->name }}</a></td>
                            <td>{{ $item->code }}</td>
                            <td>{{ $item->plan }}</td>
                            <td>{{ $item->emblemUrl != '' ? '<img src="' . $item->emblemUrl . '" width="120">' : '' }}</td>
                            
                            <td>
                               
                            </td>
                            <td>
                                
                            </td>
                        </tr>
                        @endforeach
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
    </div>

  </div>

@endsection

@section('javascript')
<script>

const traerCompetencias = () =>{

    const xhttp = new XMLHttpRequest();
    xhttp.open('GET','{{route('competencias.show')}}', true);
    xhttp.send();
    xhttp.onreadystatechange = function(){
        if( this.status == 200){
            let pruebas = JSON.parse(this.responseText);
            console.log(pruebas);
        }
    }

}

$(function(){
    $('#dataTable').DataTable();
})

</script>
@endsection