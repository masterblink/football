@extends('layout')

@section('title','Equipos')

@section('content')

<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
      <h1 class="h3 mb-0 text-gray-800">Equipos</h1>
      
    </div>

    <!-- Content Row -->
    
    <div class="row">
        <div class="col-lg-12 mb-4">

            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Equipos</h6>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                    <table class="table table-bordered" id="teams" width="90%" cellspacing="0">
                        <thead>
                        <tr>
                            
                            <th>Nombre</th>
                            <th>Codigo</th>
                            <th>Fundación</th>
                            <th>Colores</th>
                            <th>Estadio</th>
                            <th>Email</th>
                            <th>Website</th>
                        </tr>
                        </thead>
                        
                        <tbody>
                        @foreach ($equipos as $item)
                        <tr data-entry-id="{{ $item->id_team }}">
                            
                        <td><a href="{{ route('teams.detail',['id' => $item->id_team])}}">{{ $item->nombre }}</a></td>
                            <td>{{ $item->codigo}}</td>
                            <td>{{ $item->fundacion }}</td>
                            <td>{{ $item->colores }}</td>
                            <td>{{ $item->estadio }}</td>
                            <td>{{ $item->email }}</td>
                            <td>{{ $item->website }}</td>
                        </tr>
                        @endforeach  
                        
                        </tbody>
                        
                        <tfoot>
                                
                            </tfoot>
                    </table>
                    </div>
                </div>
                
            </div>
        </div>
        
    </div>

  </div>

@endsection

@section('javascript')
<script>




$(function(){

    

    $('#teams').DataTable();
    
    
})

</script>
@endsection