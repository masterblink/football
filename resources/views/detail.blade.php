@extends('layout')

@section('title','Competencias')

@section('content')

<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
      <h1 class="h3 mb-0 text-gray-800">Detalle Competencia</h1>
      <!--a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a-->
    </div>
    @if(!empty($error))
    <div class="row">
        <div class="col-lg-6 mb-4">
            <h2>Ooops... ha ocurrido un error!</h2>
            <p>{{ $error}}</p>
            <a href="{{route('competencias.index')}}">Regresar</a>
        </div>
    </div>
    @else
    <!-- Content Row -->
    <div class="row">
        <div class="col-lg-6 mb-4">
            
            <!-- Illustrations -->
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">{{ $detail['name']}}</h6>
                </div>
                <div class="card-body">
                <div class="text-center">
                    @if($detail['emblemUrl'] != 'null' && $detail['emblemUrl'] != '')
                    <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 25rem;" src="{{$detail['emblemUrl']}}" alt="">
                     
                    @endif
                </div>
                <p>
                        Nombre: {{ $detail["name"]}}<br>
                        Código: {{ $detail["code"]}}<br>
                        Plan: {{ $detail["plan"]}}<br>
                        
                </p>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 mb-4">

            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Equipos</h6>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                    <table class="table table-bordered" id="teams" width="90%" cellspacing="0">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Nombre</th>
                            <th>Codigo</th>
                            <th>Fundación</th>
                            <th>Colores</th>
                            <th>Estadio</th>
                            <th>Email</th>
                            <th>Website</th>
                        </tr>
                        </thead>
                        
                        <tbody>
                        @foreach ($detail['teams']['teams'] as $item)
                        <tr data-entry-id="{{ $item->id }}">
                            <td><input class="inpchk" type="checkbox" value="{{ $item->id }}">
                                <input id="teamval-{{ $item->id }}" type="hidden" value="{{ $item->id.','.$item->name.','.$item->tla.','.$item->founded.','.$item->clubColors.','.$item->venue.','.$item->email.','.$item->website }}">
                            </td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->tla}}</a></td>
                            <td>{{ $item->founded }}</td>
                            <td>{{ $item->clubColors }}</td>
                            <td>{{ $item->venue }}</td>
                            <td>{{ $item->email }}</td>
                            <td>{{ $item->website }}</td>
                        </tr>
                        @endforeach  
                        
                        </tbody>
                        
                        <tfoot>
                                
                            </tfoot>
                    </table>
                    </div>
                </div>
                <div class="card-footer">
                        <button id="save-team" class="btn btn-success">Guardar Equipos</button>
                        <div class="msgt"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 mb-4">

                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Jugadores</h6>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                        <table class="table table-bordered" id="players" width="90%" cellspacing="0">
                            <thead>
                            <tr>
                                <th></th>
                                <th>Nombre</th>
                                <th>Fecha Nacimiento</th>
                                <th>Pais</th>
                                <th>Nacionalidad</th>
                                <th>Posición</th>
                                <th>Nro Camiseta</th>
                            </tr>
                            </thead>
                            
                            <tbody>
                            @foreach ($detail['players']['scorers'] as $item)
                            <tr data-entry-id="{{ $item->player->id }}">
                                <td><input class="inppchk" type="checkbox" value="{{ $item->player->id }}">
                                    <input id="playerval-{{ $item->player->id }}" type="hidden" value="{{ $item->player->id.','.$item->player->name.','.$item->player->position.','.$item->player->shirtNumber }}"></td>
                                <td>{{ $item->player->name }}</td>
                                <td>{{ $item->player->dateOfBirth}}</a></td>
                                <td>{{ $item->player->countryOfBirth }}</td>
                                <td>{{ $item->player->nationality }}</td>
                                <td>{{ $item->player->position }}</td>
                                <td>{{ $item->player->shirtNumber }}</td>
                                
                            </tr>
                            @endforeach  
                            
                            </tbody>
                        </table>
                        </div>
                    </div>
                    <div class="card-footer">
                            <button id="save-player" class="btn btn-success">Guardar Jugadores</button>
                            <div class="msg"></div>
                    </div>
                </div>
            </div>
    </div>
    @endif

  </div>

@endsection

@section('javascript')
<script>




$(function(){

    function saveTeam(data){

        let params = "datos=" +JSON.stringify(data);
        const xhttp = new XMLHttpRequest();
        xhttp.open('POST','{{route('teams.save')}}', true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send(params);
        xhttp.onreadystatechange = function(){
            if( this.status == 200){
                let msg = JSON.parse(this.responseText);
                alert(msg.msg);
                $('.msgt').fadeIn(500);
                $('.msgt').html(msg.msg);

                $('.msgt').fadeOut(2000);
                $('.msgt').html('');   
                
            }
        }

    }

    function savePlayer(data){

        let params = "datos=" +JSON.stringify(data);
        const xhttp = new XMLHttpRequest();
        xhttp.open('POST','{{route('players.save')}}', true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send(params);
        xhttp.onreadystatechange = function(){
            if( this.status == 200){
                let msg = JSON.parse(this.responseText);
                alert(msg.msg);
                $('.msg').fadeIn(500);
                $('.msg').html(msg.msg);

                $('.msg').fadeOut(2000);
                $('.msg').html('');   
                
            }
            
        }

    }

    $('#teams').DataTable();
    $('#players').DataTable();

    $('#save-team').click(function(){

        let saveTeams = [],
        valor = '',
        id = '';
        $('.inpchk:checked').each(function(i,elem){
            id = $(this).val();

            valor = $('#teamval-'+id).val()

            saveTeams.push({item : valor});

        });
        
        saveTeam(saveTeams);

    });

    $('#save-player').click(function(){

        let savePlayers = [],
        valor = '',
        id = '';
        $('.inppchk:checked').each(function(i,elem){
            id = $(this).val();

            valor = $('#playerval-'+id).val()

            savePlayers.push({item : valor});

        });

        savePlayer(savePlayers);

    })
    
})

</script>
@endsection