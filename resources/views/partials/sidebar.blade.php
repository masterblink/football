<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
      <div class="sidebar-brand-icon rotate-n-15">
        <i class="fas fa-futbol"></i>
      </div>
      <div class="sidebar-brand-text mx-3">Football</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    
    <!-- Divider -->
    <hr class="sidebar-divider">


    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
    <a class="nav-link" href="{{ route('competencias.index')}}">
                <i class="fas fa-fw fa-chart-area"></i>
                <span>Competencias</span></a>  
      
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{ route('teams.index')}}">
                <i class="fas fa-fw fa-chart-area"></i>
                <span>Equipos</span></a>  
        
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{ route('players.index')}}">
                <i class="fas fa-fw fa-chart-area"></i>
                <span>Jugadores</span></a>  
        
    </li>

    

    

  </ul>