<!-- Bootstrap core JavaScript-->
<script src="{{ asset('theme/sb-admin/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{ asset('theme/sb-admin/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

<!-- Core plugin JavaScript-->
<script src="{{ asset('theme/sb-admin/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

<!-- Custom scripts for all pages-->
<script src="{{ asset('theme/sb-admin/js/sb-admin-2.min.js')}}"></script>

<!-- Page level plugins -->
<!--script src="{{-- asset('theme/sb-admin/vendor/chart.js/Chart.min.js')--}}"></script-->

<!-- Datatables -->
<script src="{{ asset('theme/sb-admin/vendor/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('theme/sb-admin/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

<!-- Page level custom scripts -->
<!--script src="js/demo/chart-area-demo.js"></script>
<script src="js/demo/chart-pie-demo.js"></script-->